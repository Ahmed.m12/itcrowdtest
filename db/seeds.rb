# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create Admins
User.create!(email: "admin_1@mailinator.com", password: "123456", password_confirmation: "123456", first_name: "admin", last_name: "one",   role: "admin")
User.create!(email: "admin_2@mailinator.com", password: "123456", password_confirmation: "123456", first_name: "admin", last_name: "two",   role: "admin")
User.create!(email: "admin_3@mailinator.com", password: "123456", password_confirmation: "123456", first_name: "admin", last_name: "three", role: "admin")
User.create!(email: "admin_4@mailinator.com", password: "123456", password_confirmation: "123456", first_name: "admin", last_name: "four",  role: "admin")
User.create!(email: "admin_5@mailinator.com", password: "123456", password_confirmation: "123456", first_name: "admin", last_name: "five",  role: "admin")


# Create Actors/Actresses
CelebUser.create!(first_name: "actor", last_name: "one",   role: "actor")
CelebUser.create!(first_name: "actor", last_name: "two",   role: "actor")
CelebUser.create!(first_name: "actor", last_name: "three", role: "actor")
CelebUser.create!(first_name: "actor", last_name: "four",  role: "actor")
CelebUser.create!(first_name: "actor", last_name: "five",  role: "actor")

# Create Directors
CelebUser.create!(first_name: "director", last_name: "one",   role: "director")
CelebUser.create!(first_name: "director", last_name: "two",   role: "director")
CelebUser.create!(first_name: "director", last_name: "three", role: "director")
CelebUser.create!(first_name: "director", last_name: "four",  role: "director")
CelebUser.create!(first_name: "director", last_name: "five",  role: "director")

# Create Producers
CelebUser.create!(first_name: "producer", last_name: "one",   role: "producer")
CelebUser.create!(first_name: "producer", last_name: "two",   role: "producer")
CelebUser.create!(first_name: "producer", last_name: "three", role: "producer")
CelebUser.create!(first_name: "producer", last_name: "four",  role: "producer")
CelebUser.create!(first_name: "producer", last_name: "five",  role: "producer")


# Create Movies
movie = Movie.create!(title: "movie_one", release_year: 2010)
# add movie actors
movie.casting << CelebUser.actors[0]
movie.casting << CelebUser.actors[1]
# add movie producer
movie.producers << CelebUser.producers[0]
# add movie director
movie.directors << CelebUser.directors[0]

movie = Movie.create!(title: "movie_two", release_year: 2012)
# add movie actors
movie.casting << CelebUser.actors[0]
movie.casting << CelebUser.actors[2]
# add movie producer
movie.producers << CelebUser.producers[1]
# add movie director
movie.directors << CelebUser.directors[1]

movie = Movie.create!(title: "movie_three", release_year: 2015)
# add movie actors
movie.casting << CelebUser.actors[3]
movie.casting << CelebUser.actors[4]
# add movie producer
movie.producers << CelebUser.producers[3]
# add movie director
movie.directors << CelebUser.directors[1]

movie = Movie.create!(title: "movie_four", release_year: 1999)
# add movie actors
movie.casting << CelebUser.actors[1]
movie.casting << CelebUser.actors[4]
# add movie producer
movie.producers << CelebUser.producers[2]
# add movie director
movie.directors << CelebUser.directors[3]

movie = Movie.create!(title: "movie_five", release_year: 2018)
# add movie actors
movie.casting << CelebUser.actors[1]
movie.casting << CelebUser.actors[4]
# add movie producer
movie.producers << CelebUser.producers[3]
# add movie director
movie.directors << CelebUser.directors[2]