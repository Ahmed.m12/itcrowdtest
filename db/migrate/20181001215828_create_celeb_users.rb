class CreateCelebUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :celeb_users do |t|
      t.string :first_name
      t.string :last_name

      t.integer :role
    end
  end
end
