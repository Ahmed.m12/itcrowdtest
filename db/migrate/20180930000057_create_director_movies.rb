class CreateDirectorMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :director_movies do |t|
      t.belongs_to :celeb_user, index: true
      t.belongs_to :movie, index: true

      t.timestamps null: false
    end
  end
end
