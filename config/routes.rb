Rails.application.routes.draw do
  root 'movies#index'
  
  devise_for :users, controllers: { registrations: "users/registrations" }
  
  resources :movies

  resources :celeb_users, only: [:show, :new, :create, :edit, :update, :destroy] do
    collection do
      get :actors
      get :producers
      get :directors
    end
  end

  # version 1
  api_version(:module => "V1", :path => {:value => "v1"}, :defaults => {:format => :json}) do

    mount_devise_token_auth_for 'User', at: 'users', controllers: { registrations: 'v1/users/registrations' }

    resources :movies

    resources :celeb_users, only: [:show, :new, :create, :edit, :update, :destroy] do
      collection do
        get :actors
        get :producers
        get :directors
      end
    end
  end
end
