class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  
  include DeviseTokenAuth::Concerns::User

  enum role: [:normal_user, :admin]

  scope :normal_users, -> { where(role: :normal_user) }
  scope :admins, -> { where(role: :admin) }

  def full_name
    first_name + " " + last_name
  end

  after_create :confirm_user

  def confirm_user
    self.confirm
  end
end
