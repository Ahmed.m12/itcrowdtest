class ActorMovie < ApplicationRecord
  belongs_to :actor, class_name: "CelebUser", foreign_key: "celeb_user_id"
  belongs_to :movie
end