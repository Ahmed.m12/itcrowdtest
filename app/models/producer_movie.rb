class ProducerMovie < ApplicationRecord
  belongs_to :producer, class_name: "CelebUser", foreign_key: "celeb_user_id"
  belongs_to :movie
end