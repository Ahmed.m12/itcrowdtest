class CelebUser < ApplicationRecord
  has_many :actor_movies
  has_many :movies_as_actor, through: :actor_movies, source: :movie

  has_many :director_movies
  has_many :movies_as_director, through: :director_movies, source: :movie

  has_many :producer_movies
  has_many :movies_as_producer, through: :producer_movies, source: :movie

  enum role: [:actor, :producer, :director]

  scope :actors, -> { where(role: :actor) }
  scope :producers, -> { where(role: :producer) }
  scope :directors, -> { where(role: :director) }

  def full_name
    first_name + " " + last_name
  end

end
