class DirectorMovie < ApplicationRecord
  belongs_to :director, class_name: "CelebUser", foreign_key: "celeb_user_id"
  belongs_to :movie
end