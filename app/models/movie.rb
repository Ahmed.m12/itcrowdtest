class Movie < ApplicationRecord
  has_many :actor_movies, dependent: :destroy
  has_many :casting, through: :actor_movies, source: :actor
  accepts_nested_attributes_for :actor_movies, allow_destroy: true
  
  has_many :director_movies, dependent: :destroy
  has_many :directors, through: :director_movies, source: :director
  accepts_nested_attributes_for :director_movies, allow_destroy: true

  has_many :producer_movies, dependent: :destroy
  has_many :producers, through: :producer_movies, source: :producer
  accepts_nested_attributes_for :producer_movies, allow_destroy: true

  validates :title, :release_year, presence: true

  validates :title, length: { maximum: 50, message: "should be at most 50 characters" }, allow_blank: true

  validates :release_year, numericality: {greater_than: 0}, allow_blank: true
end