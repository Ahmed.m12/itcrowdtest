class CelebUserSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :role

  attribute :movies_as_casting,  if: ->{celeb_user_is_casting?}
  attribute :movies_as_director, if: ->{celeb_user_is_director?}
  attribute :movies_as_producer, if: ->{celeb_user_is_producer?}

  def celeb_user_is_casting?
    self.object.actor?
  end

  def celeb_user_is_producer?
    self.object.producer?
  end

  def celeb_user_is_director?
    self.object.director?
  end

  def movies_as_casting
    self.object.movies_as_actor.map do |movie|
      [movie.title, scope.movie_url(movie.id)]
    end
  end

  def movies_as_director
    self.object.movies_as_director.map do |movie|
      [movie.title, scope.movie_url(movie.id)]
    end
  end

  def movies_as_producer
    self.object.movies_as_producer.map do |movie|
      [movie.title, scope.movie_url(movie.id)]
    end
  end
end