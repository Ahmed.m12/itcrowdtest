class MovieSerializer < ActiveModel::Serializer
  attributes :id, :title, :release_year

  has_many :casting, serializer: CelebUserSerializer
  has_many :directors, serializer: CelebUserSerializer
  has_many :producers, serializer: CelebUserSerializer
  
  def release_year
    RomanNumerals.to_roman(self.object.release_year)
  end

end