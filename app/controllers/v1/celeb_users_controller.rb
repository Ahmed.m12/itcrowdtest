class V1::CelebUsersController < V1::BaseController
  before_action :authenticate_user!, except: [:show, :actors, :directors, :producers]
  before_action :authenticate_user_as_admin, only: [:create, :update, :destroy]
  before_action :set_celeb_user, only: [:show, :update, :destroy]
  serialization_scope :view_context

  def actors
    @celeb_users = CelebUser.actors
    render json: @celeb_users
  end

  def directors
    @celeb_users = CelebUser.directors
    render json: @celeb_users
  end

  def producers
    @celeb_users = CelebUser.producers
    render json: @celeb_users
  end

  def show
    render json: @celeb_user
  end

  def create
    @celeb_user = CelebUser.new(celeb_user_params)

    if @celeb_user.save
      render json: @celeb_user
    else
      render json: @celeb_user.errors, status: :unprocessable_entity
    end
  end

  def update
    # Cause celebuser role cannot be changed
    params[:celeb_user][:role] = @celeb_user.role
    
    if @celeb_user.update(celeb_user_params)
      render json: @celeb_user
    else
      render json: @celeb_user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @celeb_user.destroy
    render json: {success: true, message: "Celeb User has been removed successfully."}
  end


  private
    def set_celeb_user
      @celeb_user = CelebUser.find(params[:id])
    end

    def celeb_user_params
      params.require(:celeb_user).permit(:first_name, :last_name, :role)
    end

end