class V1::MoviesController < V1::BaseController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authenticate_user_as_admin, only: [:new, :create, :update, :destroy]
  before_action :set_movie, only: [:show, :edit, :update, :destroy]
  serialization_scope :view_context

  def index
    @movies = Movie.all
    render json: @movies
  end

  def show
    render json: @movie
  end

  def new
    @movie = Movie.new
  end

  def edit
  end

  def create
    @movie = Movie.new(movie_params)

    if @movie.save
      render json: @movie
    else
      render json: @movie.errors, status: :unprocessable_entity
    end
  end

  def update
    if @movie.update(movie_params)
      render json: @movie
    else
      render json: @movie.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @movie.destroy
    render json: {success: true, message: "Movie has been removed successfully."}
  end

  private
    def set_movie
      @movie = Movie.find(params[:id])
    end

    def movie_params
      params.require(:movie).permit(:title, :release_year, 
                                    actor_movies_attributes: [:id, :celeb_user_id, :_destroy], 
                                    producer_movies_attributes: [:id, :celeb_user_id, :_destroy], 
                                    director_movies_attributes: [:id, :celeb_user_id, :_destroy])
    end
end