class V1::BaseController < ActionController::API
  protect_from_forgery with: :null_session
  include DeviseTokenAuth::Concerns::SetUserByToken

  def authenticate_user_as_admin
    return if current_user.admin?
    render json: {success: false, message: "You don\'t have permission!"}
  end
end