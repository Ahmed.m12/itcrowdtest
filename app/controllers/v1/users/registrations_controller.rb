module V1
  class Users::RegistrationsController < DeviseTokenAuth::RegistrationsController

    private
      def sign_up_params
        params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
      end

      def account_update_params
        params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
      end
  end
end