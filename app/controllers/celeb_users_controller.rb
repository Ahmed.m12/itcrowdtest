class CelebUsersController < ApplicationController
  before_action :authenticate_user!, except: [:show, :actors, :directors, :producers]
  before_action :authenticate_user_as_admin, only: [:new, :create, :update, :destroy]
  before_action :set_celeb_user, only: [:show, :edit, :update, :destroy]
  serialization_scope :view_context

  def new
    @celeb_user = CelebUser.new
  end
  
  def actors
    @celeb_users = CelebUser.actors
  end

  def directors
    @celeb_users = CelebUser.directors
  end

  def producers
    @celeb_users = CelebUser.producers
  end

  def show
  end

  def create
    @celeb_user = CelebUser.new(celeb_user_params)

    if @celeb_user.save
      redirect_to @celeb_user, notice: 'Celeb User was successfully created.'
    else
      render :new
    end
  end

  def update
    # Cause celebuser role cannot be changed
    params[:celeb_user][:role] = @celeb_user.role
    
    if @celeb_user.update(celeb_user_params)
      redirect_to @celeb_user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @celeb_user.destroy
      redirect_to users_url, notice: 'User was successfully destroyed.'
  end


  private
    def set_celeb_user
      @celeb_user = CelebUser.find(params[:id])
    end

    def celeb_user_params
      params.require(:celeb_user).permit(:first_name, :last_name, :role)
    end

end