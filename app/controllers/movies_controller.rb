class MoviesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authenticate_user_as_admin, only: [:new, :create, :update, :destroy]
  before_action :set_movie, only: [:show, :edit, :update, :destroy]
  serialization_scope :view_context

  def index
    @movies = Movie.all
  end

  def show
  end

  def new
    @movie = Movie.new
  end

  def edit
  end

  def create
    @movie = Movie.new(movie_params)

    if @movie.save
      redirect_to @movie, notice: 'Movie was successfully created.'
    else
      render :new
    end
  end

  def update
    if @movie.update(movie_params)
      redirect_to @movie, notice: 'Movie was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @movie.destroy
      redirect_to movies_url, notice: 'Movie was successfully destroyed.'
  end

  private
    def set_movie
      @movie = Movie.find(params[:id])
    end

    def movie_params
      params.require(:movie).permit(:title, :release_year, 
                                    actor_movies_attributes: [:id, :celeb_user_id, :_destroy], 
                                    producer_movies_attributes: [:id, :celeb_user_id, :_destroy], 
                                    director_movies_attributes: [:id, :celeb_user_id, :_destroy])
    end
end