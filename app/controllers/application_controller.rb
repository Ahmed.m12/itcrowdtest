class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  def authenticate_user_as_admin
    return if current_user.admin?
    flash[:alert] = "You don\'t have permission!"
    redirect_to root_url
  end
end
